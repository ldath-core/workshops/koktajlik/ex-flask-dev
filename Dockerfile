FROM python:3.10-alpine

ENV FLASK_APP api.py
ENV FLASK_CONFIG production

RUN apk add --no-cache bash

RUN adduser -D api

WORKDIR /home/api

COPY requirements requirements
RUN python -m venv venv
RUN set -ex \
    && apk add --no-cache --virtual .build-deps \
        build-base python3-dev libffi-dev gcc musl-dev make \
    && /home/api/venv/bin/python -m pip install --no-cache-dir wheel \
    && /home/api/venv/bin/python -m pip install --no-cache-dir -r requirements/docker.txt \
    && apk del .build-deps

RUN chown -R api:api /home/api
USER api

COPY app app
COPY api.py config.py boot.sh cmd.sh ./

# run-time configuration
EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
CMD ["./cmd.sh"]